//
//  ViewA.swift
//  iosBasics
//
//  Created by SushantNeupane on 9/10/22.
//

import SwiftUI

struct ViewA: View {
    var body: some View {
        ZStack{
            Color.red
            
            Image(systemName: "phone.fill")
                .foregroundColor(.white)
                .font(.system(size:100.0))
        }
    }
}

struct ViewA_Previews: PreviewProvider {
    static var previews: some View {
        ViewA()
    }
}
